#!/bin/bash

echo $(tput setaf 3)'Trying to stop sentinels...'$(tput sgr0)
failed=0

redis-cli -p 5001 --no-auth-warning --user default -a dont_hack_anything shutdown
[ ! $? -eq 0 ] && echo 'Failed to stop sentinel-1!' && ((failed++))

redis-cli -p 5002 --no-auth-warning --user default -a dont_hack_anything shutdown
[ ! $? -eq 0 ] && echo 'Failed to stop sentinel-2!' && ((failed++))

redis-cli -p 5003 --no-auth-warning --user default -a dont_hack_anything shutdown
[ ! $? -eq 0 ] && echo 'Failed to stop sentinel-3!' && ((failed++))

if [ $failed -eq 0 ]; then
  echo $(tput setaf 2)'All sentinels are stopped!'$(tput sgr0)
else
  echo $(tput setaf 1)'Failed to stop sentinels!'$(tput sgr0)
fi

ps -ef | grep "[r]edis-sentinel"

