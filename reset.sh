#!/bin/sh

# clear data
rm -f redis-*/*.aof
rm -f redis-*/*.rdb
rm -f redis-*/*.log
rm -f sentinel-*/*.log

# reset config files with '-c' option
if [ "$1" = "-c" ]; then
  git checkout -- redis-*
  git checkout -- sentinel-*
fi

