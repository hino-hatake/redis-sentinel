#!/bin/sh

[ -n "$1" ] && LINES=$1 || LINES=10

echo $(tput setaf 3)'sentinel-1:'$(tput sgr0)
tail -n $LINES sentinel-1/sentinel.log
echo $(tput setaf 3)'sentinel-2:'$(tput sgr0)
tail -n $LINES sentinel-2/sentinel.log
echo $(tput setaf 3)'sentinel-3:'$(tput sgr0)
tail -n $LINES sentinel-3/sentinel.log

