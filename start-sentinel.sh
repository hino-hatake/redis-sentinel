#!/bin/sh

echo $(tput setaf 3)'Checking if redis sentinels were running...'$(tput sgr0)

if lsof -nPi:5001 -sTCP:LISTEN > /dev/null \
  && lsof -nPi:5002 -sTCP:LISTEN > /dev/null \
  && lsof -nPi:5003 -sTCP:LISTEN > /dev/null; then

  echo $(tput setaf 1)'Already running!'$(tput sgr0)
  ps -ef | grep "[r]edis-sentinel"
  exit 0
fi

echo $(tput setaf 3)'Trying to start sentinels...'$(tput sgr0)

cd sentinel-1 && redis-sentinel sentinel.conf
[ ! $? -eq 0 ] && echo $(tput setaf 1)'Failed to start sentinel-1!'$(tput sgr0)

cd ../sentinel-2 && redis-sentinel sentinel.conf
[ ! $? -eq 0 ] && echo $(tput setaf 1)'Failed to start sentinel-2!'$(tput sgr0)

cd ../sentinel-3 && redis-sentinel sentinel.conf
[ ! $? -eq 0 ] && echo $(tput setaf 1)'Failed to start sentinel-3!'$(tput sgr0)

echo $(tput setaf 2)'All sentinels are running!'$(tput sgr0)
ps -ef | grep "[r]edis-sentinel"

