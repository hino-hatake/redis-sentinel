#!/bin/sh

[ -n "$1" ] && LINES=$1 || LINES=10

echo $(tput setaf 3)'redis-1:'$(tput sgr0)
tail -n $LINES redis-1/redis.log
echo $(tput setaf 3)'redis-2:'$(tput sgr0)
tail -n $LINES redis-2/redis.log
echo $(tput setaf 3)'redis-3:'$(tput sgr0)
tail -n $LINES redis-3/redis.log

