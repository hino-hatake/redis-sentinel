#!/bin/bash

echo $(tput setaf 3)'Trying to stop redis servers...'$(tput sgr0)
failed=0

redis-cli -p 6001 --no-auth-warning --user mera -a dont_hack_us shutdown
[ ! $? -eq 0 ] && echo 'Failed to stop redis-1!' && ((failed++))

redis-cli -p 6002 --no-auth-warning --user mera -a dont_hack_us shutdown
[ ! $? -eq 0 ] && echo 'Failed to stop redis-2!' && ((failed++))

redis-cli -p 6003 --no-auth-warning --user mera -a dont_hack_us shutdown
[ ! $? -eq 0 ] && echo 'Failed to stop redis-3!' && ((failed++))

if [ $failed -eq 0 ]; then
  echo $(tput setaf 2)'All redis servers are stopped!'$(tput sgr0)
else
  echo $(tput setaf 1)'Failed to stop redis servers!'$(tput sgr0)
fi

ps -ef | grep "[r]edis-server"

