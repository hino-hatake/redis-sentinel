#!/bin/sh

# get IP addr from 1st argument if specified
[ -n "$1" ] && IP=$1 || IP=127.0.0.1

HOSTNAMES='redis-1 redis-2 redis-3 sentinel-1 sentinel-2 sentinel-3'

if grep -q "$HOSTNAMES" /etc/hosts; then
  echo $(tput setaf 3)'Already configured!'$(tput sgr0)
  grep "$HOSTNAMES" /etc/hosts
  exit 0
fi

echo $IP' '$HOSTNAMES | sudo tee -a /etc/hosts > /dev/null

echo $(tput setaf 2)'Hostnames configured!'$(tput sgr0)
grep "$HOSTNAMES" /etc/hosts

