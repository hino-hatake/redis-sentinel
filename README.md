## Prerequisites
- Redis 6.2.2

## Tips to operate, but up to you
### 1. hostname config
```bash
echo '127.0.0.1 redis-1 redis-2 redis-3 sentinel-1 sentinel-2 sentinel-3' | sudo tee -a /etc/hosts
```

### 2. start n stop
```bash
./start-redis.sh
./stop-redis.sh
```

### 3. set n get
```bash
./1.sh set name hino
./2.sh keys '*'
./3.sh get name
```

### 4. sentinel cli
```bash
./4.sh info sentinel
./5.sh sentinel failover trident
./6.sh sentinel master trident
```
