#!/bin/sh

echo $(tput setaf 3)'Checking if redis servers were running...'$(tput sgr0)

if lsof -nPi:6001 -sTCP:LISTEN > /dev/null \
  && lsof -nPi:6002 -sTCP:LISTEN > /dev/null \
  && lsof -nPi:6003 -sTCP:LISTEN > /dev/null; then

  echo $(tput setaf 1)'Already running!'$(tput sgr0)
  ps -ef | grep "[r]edis-server"
  exit 0
fi

echo $(tput setaf 3)'Trying to start redis servers...'$(tput sgr0)

cd redis-1 && redis-server redis.conf
[ ! $? -eq 0 ] && echo $(tput setaf 1)'Failed to start redis-1!'$(tput sgr0)

cd ../redis-2 && redis-server redis.conf
[ ! $? -eq 0 ] && echo $(tput setaf 1)'Failed to start redis-2!'$(tput sgr0)

cd ../redis-3 && redis-server redis.conf
[ ! $? -eq 0 ] && echo $(tput setaf 1)'Failed to start redis-3!'$(tput sgr0)

echo $(tput setaf 2)'All redis servers are running!'$(tput sgr0)
ps -ef | grep "[r]edis-server"

