#!/bin/sh

PORT=5003
USER=default
PASSWORD=dont_hack_anything
redis-cli -p $PORT --user $USER -a $PASSWORD --no-auth-warning "$@"

